# -*- coding: utf-8 -*-
import asyncio
import time

from services.pulsar_producer import PulsarProducer

from similarity import SimilarityMessagesBackground
producer = PulsarProducer()
print('Start LeadsMonitoringMessages')
producer.connect()
cls = SimilarityMessagesBackground(producer)
asyncio.run(cls.run())
print('!!!!!!!!!!!!!!!!!')
