import json

import pulsar

# Опции для отключения проверки SSL
client_opts = {
    #'web_service_url': 'http://messenger.teleseller.ru:6650',  # Замените на ваш URL
    'use_tls': True,
}


class PulsarProducer:
    _instance = None  # Статическая переменная для хранения единственного экземпляра

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super().__new__(cls, *args, **kwargs)
            cls._instance._initialized = False
        return cls._instance

    def __init__(self):
        # ssl._create_default_https_context = ssl._create_unverified_context
        self.broker_url = 'pulsar://messenger.teleseller.ru:6650'
        self.topic = 'my-topic-new'
        self.topic_scan_history = 'scan_history'
        self.topic_process = 'my-topic-process'
        if not self._initialized:
            self.client: pulsar.Client = None
            self.producer: pulsar.Producer = None
            self.producer_process: pulsar.Producer = None
            print('INIT!!!')
        self._initialized = True

    def connect(self):
        try:
            self.client = pulsar.Client(self.broker_url, **client_opts) #, **client_opts
            self.producer = self.client.create_producer(self.topic)
            self.producer_process = self.client.create_producer(self.topic_process)
            print('Connected to Pulsar broker. Topic:', self.topic)

        except Exception as e:
            print('Failed to connect to Pulsar:', str(e))


    async def send_message_process(self, message):
        try:
            message_id = self.producer_process.send(json.dumps(message).encode('utf-8'))
            print(f"Message sent process. Message ID: {message_id}")
        except pulsar.PulsarException as e:
            print('Failed to send message:', str(e))
            print('Attempting to reconnect...')
            self.close()
            self.connect()
        except Exception as e:
            print('Failed to send message:', str(e))


    async def send_message(self, message):
        try:
            message_id = self.producer.send(json.dumps(message).encode('utf-8'))
            ## print('Message sent. Message ID:' + message_id)
        except pulsar.PulsarException as e:
            print('Failed to send message:', str(e))
            print('Attempting to reconnect...')
            self.close()
            self.connect()
        except Exception as e:
            print('Failed to send message:', str(e))


    def close(self):
        if self.producer:
            self.producer.close()
        if self.producer_process:
            self.producer_process.close()
        if self.client:
            self.client.close()
