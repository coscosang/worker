from setuptools import setup
import subprocess
PACKAGES = []


APP = ['main.py']
DATA_FILES = []
# Список зависимостей
dependencies = [
    #'charset-normalizer',
    #'asyncio',
    'numpy',
    'pulsar',
    'tensorflow-macos',
    'tensorflow-metal',
    'tensorflow_hub',
    'tensorflow_text',
    'pandas',
    #'chardet',
]

# Установка зависимостей
"""for dependency in dependencies:
    subprocess.call(['pip', 'install', dependency])"""
OPTIONS = {
    'argv_emulation': True,
    'packages': ['asyncio', 'ml_dtypes', 'numpy', 'pulsar', 'tensorflow_hub', 'tensorflow', 'tensorflow_hub', 'tensorflow_text', 'pandas']  # Поменяйте на название вашего пакета
}
setup(
    app=APP,
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    setup_requires=['py2app'],
    install_requires=dependencies
)