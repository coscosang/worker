import asyncio
import json
import os
import time
from dotenv import load_dotenv
import socket
import subprocess
import sys
import threading
from typing import List
import numpy as np
import tensorflow_hub as hub
import tensorflow_text
import tensorflow as tf
from _pulsar import ConsumerType
from colorama import Fore, Back, Style, init

from services.pulsar_producer import PulsarProducer

import requests

os.environ['TFHUB_DOWNLOAD_PROGRESS'] = "1"


class SimilarityMessagesBackground():
    similarity_min_len = 15
    similarity = 0.4
    count = 0
    all_count = 0
    all_count_history = 0
    count_history = 0

    def __init__(self, producer: PulsarProducer):
        super().__init__()
        self.producer: PulsarProducer = producer
        self.model = None
        self.advertisements: List[str] = []
        # Загрузить переменные среды из файла .env
        load_dotenv()
        # Получить значение переменной среды
        self.timeout = float(os.getenv("TIMEOUT", 0.01))
        print('TIMEOUT', self.timeout)


        # Установить количество потоков для операций на CPU
        tf.config.threading.set_inter_op_parallelism_threads(1)
        tf.config.threading.set_intra_op_parallelism_threads(1)

    async def periodic_task(self):
        start_time = time.time()
        while True:
            advertisements = self.load_advertisements()
            if len(advertisements) > 0:
                self.advertisements = advertisements

            # Check elapsed time without delaying the loop
            elapsed_time = time.time() - start_time
            if elapsed_time > 3600:  # 1 hour = 3600 seconds
                os._exit(0)
            await asyncio.sleep(120)

    async def run(self):
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
        os.environ['TFHUB_DOWNLOAD_PROGRESS'] = '1'
        status_string = f"{Fore.GREEN}Загрузка модели: 600 mb...{Style.RESET_ALL}"
        print(status_string)
        self.model = hub.load("https://tfhub.dev/google/universal-sentence-encoder-multilingual-large/3")

        status_string = f"{Fore.GREEN}Модель загружена:{Style.RESET_ALL}"
        print(status_string)
        await asyncio.sleep(3)
        self.advertisements = self.load_advertisements()
        # Создаем потребителя для темы
        thread = threading.Thread(target=self.start_async_method, args=(self.subscribe_topic_messages(),))
        thread.start()
        thread2 = threading.Thread(target=self.start_async_method, args=(self.subscribe_topic_history(),))
        thread2.start()

        await asyncio.sleep(120)
        asyncio.create_task(self.periodic_task())
        await asyncio.sleep(3600)
        try:
            self.producer.client.close()
            self.producer.close()
        except Exception as e:
            pass
        # Выполняем команду git pull
        subprocess.run(['git', 'pull'])
        # Получаем текущий путь скрипта
        current_path = os.path.abspath(sys.argv[0])
        # Перезапускаем себя с использованием текущего интерпретатора Python
        print('EXIT')
        os.execl(sys.executable, sys.executable, current_path)

    async def check_keywords(self, text, keywords, exclusions, threshold=0.8):
        doc = self.nlp(text.lower())
        lemmas = [token.lemma_ for token in doc if token.is_alpha and token.lemma_]

        # Рассчет процента совпадения с ключевыми словами
        matching_percentage = sum(1 for lemma in lemmas if lemma in keywords) / len(keywords)

        # Проверка процента совпадения и отсутствия исключений
        return matching_percentage >= threshold and all(lemma not in exclusions for lemma in lemmas)

    def calculate_similarity(self, comparison_strings: List[str], target_string: str) -> List[int]:
        # Получить векторные представления для строк
        embeddings = self.model([target_string] + comparison_strings)

        # Векторное представление целевой строки
        target_embedding = embeddings[0]

        # Векторные представления строк для сравнения
        comparison_embeddings = embeddings[1:]

        # Вычислить сходство между target_embedding и каждым comparison_embedding
        similarities = np.dot(target_embedding, np.transpose(comparison_embeddings))

        # print(similarities)
        return similarities

    async def compare_to_advertisements(self, message, advertisements: List[str]) -> bool:
        # Примеры объявлений
        exist = False
        if len(message['text']) > 15:
            similarity_score = self.calculate_similarity(advertisements, message['text'])
            # print(similarity_score, message['text'])
            for index, scope in enumerate(similarity_score):
                ad = advertisements[index]
                if len(ad) > 10 and scope > self.similarity:
                    print(
                        f"{Fore.GREEN}Похожесть сообщения на объявление - '{ad[:30]}' : {scope} | {message['text'][:50]}{Style.RESET_ALL}")
                    asyncio.create_task(self.producer.send_message_process({
                        'message': message,
                        'scope': float(scope),
                        'ad': ad,
                    }))
                    exist = True
                    # await self.telegram_api.sendLog(text)
        return exist

    def load_advertisements(self) -> List[str]:
        url = "https://messenger.teleseller.ru:8001/api/lead/advertisements"
        try:
            response = requests.get(url)
            if response.status_code == 200:
                data = response.json()  # Получаем данные в формате JSON
                status_string = f"{Fore.GREEN}Получено {len(data)} текстов для сверки нейросетью:{Style.RESET_ALL}"
                print(status_string)
                return data
            else:
                print("Request failed with status code:", response.status_code)
        except requests.exceptions.RequestException as e:
            print("An error occurred:", e)
        return []

    async def subscribe_topic_messages(self):
        print('Start subscribe topic')
        consumer = self.producer.client.subscribe(self.producer.topic, 'subscription',
                                                  consumer_type=ConsumerType.Shared)
        while True:
            # Получаем сообщение из исходной темы
            msg = consumer.receive()
            consumer.acknowledge(msg)
            self.all_count += 1
            await asyncio.sleep(self.timeout)
            data = msg.data().decode('utf-8')
            message = json.loads(data)
            status_string = f"Обрабатывается сообщение #{message['id']}, обработано {self.all_count}, добыто {self.count}"
            print(status_string)
            if await self.compare_to_advertisements(message, self.advertisements):
                self.count += 1
                # display_status(self.count)


    async def subscribe_topic_history(self):
        print('Start subscribe topic_scan_history')
        consumer = self.producer.client.subscribe(self.producer.topic_scan_history, 'subscription',
                                                  # -' + get_hostname(),
                                                  consumer_type=ConsumerType.Shared)
        while True:
            # Получаем сообщение из исходной темы
            msg = consumer.receive()
            consumer.acknowledge(msg)
            self.all_count_history += 1
            await asyncio.sleep(self.timeout)
            data = msg.data().decode('utf-8')
            message = json.loads(data)

            if await self.compare_to_advertisements(message, message['phrases']):
                self.count_history += 1
                # display_status(self.count)
            status_string = f"{Fore.GREEN}Обрабатывается сообщение истории #{message['id']}, обработано {self.all_count_history}, добыто {self.count_history}{Style.RESET_ALL}"
            print(status_string)

    def start_async_method(self, method):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_until_complete(method)
        loop.close()



def display_status(count):
    # Форматируем строку для вывода
    status_string = f"{Fore.GREEN}Выполнено задач: {count}{Style.RESET_ALL}"
    print(status_string)
