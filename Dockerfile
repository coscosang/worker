FROM python:3.10
ENV PYTHONUNBUFFERED 1
# Используем "bash" вместо "sh"
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Установка зависимостей
RUN apt-get update && apt-get install -y \
    software-properties-common \
    python3-pip \
    python3-dev \
    libldap2-dev \
    libsasl2-dev \
    libssl-dev \
    locales \
    lsb-release \
    gcc

# Обновление локали

RUN set -x && \
    apt-get update -y && \
    apt-get upgrade -y  && \
    apt-get install -y  && \
    echo ru_RU.UTF-8 UTF-8 >> /etc/locale.gen && \
    locale-gen

ENV APPDIR /backend
RUN mkdir -p $APPDIR
WORKDIR $APPDIR

# Копирование requirements.txt внутрь контейнера
COPY requirements.txt $APPDIR/

# Установка зависимостей из requirements.txt
RUN pip3 install --no-cache-dir --upgrade -r requirements.txt
